#define SCL 3
#define SDA 4

#define SCL_INPUT  DDRD &= ~(1<<DDB3)// <=>  pinMode(SCL,INPUT);
#define SCL_OUTPUT DDRD|= (1<<DDB3) // <=>  pinMode(SLC,OUTPUT);
#define SDA_INPUT  DDRD &= ~(1<<DDB4)// <=>  pinMode(SDA,INPUT);
#define SDA_OUTPUT DDRD|= (1<<DDB4) // <=>  pinMode(SDA,OUTPUT);

#define SDA_HIGH PORTD |= (1<<PD4) // digitalWrite(SDA,HIGH);
#define SCL_HIGH PORTD |= (1<<PD3)
#define SDA_LOW PORTD &= ~(1<<PD4)
#define SCL_LOW PORTD &= ~(1<<PD3)

#define SDA_read  ( PIND & ( 1<<PIND4) )>>PIND4
#define SCL_read  ( PIND & ( 1<<PIND3) )>>PIND3

#define T_us 50

void setup() {
  // put your setup code here, to run once:
Serial.begin(9600);
uint8_t data;
data=I2C_read(0);
Serial.println(data);
}

void loop() {
  // put your main code here, to run repeatedly:


}
uint8_t I2C_read(byte address)
{
  uint8_t data=0,i;
  while (true)
  {
    SDA_INPUT;
    SCL_INPUT;
    while((SDA_read!=1)||(SCL_read!=1)); // xét SDA và SCL đang ở vị trí bắt đầu
    while(SDA_read==1);// đợi SDA xuống mức 0 trước
    delayMicroseconds(T_us/4);
    if(SCL_read==1){
      while(SCL_read==1);//  đợi SCL xuosng mức 1 để vào trạng thái bắt đầu
      for ( i = 0; i < 8; i++)
      {
        while(SCL_read==0); // Đợi SCL lên mức 1
        data=data<<1 | SDA_read;
        while(SCL_read==1); // Đợi SCL xuống mức 0 kết thúc 1 xung
      }
      SDA_OUTPUT;
      SDA_LOW;
      while(SCL_read==0);// Combo 1 xung = 2 câu lệnh while này
      while(SCL_read==1);//
      SDA_INPUT;
      return data;
    }
  }
}
