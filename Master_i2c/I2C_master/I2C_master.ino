// #include<Wire.h>
#define SCL 3
#define SDA 4

#define SCL_INPUT  DDRD &= ~(1<<DDB3)// <=>  pinMode(SCL,INPUT);
#define SCL_OUTPUT DDRD|= (1<<DDB3) // <=>  pinMode(SLC,OUTPUT);
#define SDA_INPUT  DDRD &= ~(1<<DDB4)// <=>  pinMode(SDA,INPUT);
#define SDA_OUTPUT DDRD|= (1<<DDB4) // <=>  pinMode(SDA,OUTPUT);

#define SDA_HIGH PORTD |= (1<<PD4) // digitalWrite(SDA,HIGH);
#define SCL_HIGH PORTD |= (1<<PD3)
#define SDA_LOW PORTD &= ~(1<<PD4)
#define SCL_LOW PORTD &= ~(1<<PD3)

#define SDA_read  ( PIND & ( 1<<PIND4) )>>PIND4
#define SCL_read  ( PIND & ( 1<<PIND3) )>>PIND3

#define T_us 50
void setup() {
  // put your setup code here, to run once:
// SDA_HIGH;
// SCL_HIGH;
// SDA_OUTPUT;
// SCL_OUTPUT;
//
// delay(1000);
// beginI2C(0);
  // SDA_HIGH;
  // SCL_HIGH;
  delay(100);
  I2C_Writedata(0x55,"abc");
}

void loop() {
  // put your main code here, to run repeatedly:
  // uint8_t ack;
  //  I2C_Start();
  //  I2C_Send_Byte(0xAA);
  //  ack=read_ACK();
  //  if(ack==1)I2C_Stop();
  //  else I2C_Send_Byte('S');
  //  I2C_Stop();
  // I2C_Writedata(0x55,"abc");

}

void I2C_Start(void)
{
   // Start: SDA chuyển từ mức điện áp cao xuống mức điện áp thấp trước khi đường SCL chuyển từ mức cao xuống mức thấp
    SDA_HIGH;
    SCL_HIGH;
    SDA_OUTPUT;
    SCL_OUTPUT;
    SDA_LOW; delayMicroseconds(T_us/2);
    SCL_LOW; delayMicroseconds(T_us/2);
}
void I2C_Stop(void)
{
  // SDA chuyển từ mức điện áp thấp sang mức điện áp cao sau khi đường SCL chuyển từ mức thấp lên mức cao
  SDA_OUTPUT;
  SDA_LOW;
  SCL_LOW;
  delayMicroseconds(T_us/2);
  SCL_HIGH;
  delayMicroseconds(T_us/2);
  SDA_HIGH;
}
uint8_t read_ACK()
{
  SDA_INPUT;
  delayMicroseconds(T_us/2);
  SCL_HIGH;
  delayMicroseconds(T_us/2);
  uint8_t ack=SDA_read;// đọc giá trị của phản hồi của slave
  SCL_LOW;
  delayMicroseconds(T_us/2);
  return ack;
}
void I2C_Send_Byte(uint8_t data)
{
  uint8_t i=0;
  SDA_OUTPUT;
  for(i;i<8;i++)
  {
    if((data & 0x80) !=0)
      SDA_HIGH;
    else
      SDA_LOW;
    data= data<<1;
    delayMicroseconds(T_us/2);
    SCL_HIGH;
    delayMicroseconds(T_us/2);
    SCL_LOW;
  }
}
uint8_t I2C_Writedata(uint8_t address, char *data)
{
  uint8_t ack;
  //1: START
  I2C_Start();
  //2 : send address
  address=address<<1;
  I2C_Send_Byte(address);
  ack=read_ACK();
  if(ack==1)
  {
    I2C_Stop();
    return -1;
  }
  //3 :send data
  while (*data!='\0')
  {
    I2C_Send_Byte(*data);
    ack=read_ACK();
    if(ack==1)
    {
      I2C_Stop();
      return -1;
    }
    data++;
  }
  // 4: Stop
  I2C_Stop();
  return 1;
}
